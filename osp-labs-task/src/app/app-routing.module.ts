import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo:'site', pathMatch:'full'},
   {
    path:`site`,
    loadChildren:() => import('./site/site.module').then(m=>m.SiteModule)
  },
  {
    path:`auth`,
    loadChildren:() => import('./auth/auth.module').then(m=>m.AuthModule)
  },
  {
    path:`admin`,
    loadChildren:() => import('./site/site.module').then(m=>m.SiteModule)
  },
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
