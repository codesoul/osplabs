import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SiteComponent } from './container/site/site.component';
import { LandingPageComponent } from './pages/landing-page/landing-page.component';
import { SiteRoutingModule } from './sites-routing.module';


@NgModule({
  declarations: [
    SiteComponent,
    LandingPageComponent
  ],
  imports: [
    CommonModule,
    SiteRoutingModule
  ]
})
export class SiteModule { }
