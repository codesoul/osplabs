import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SiteComponent } from './container/site/site.component';
import { LandingPageComponent } from './pages/landing-page/landing-page.component';

const routes: Routes = [
   {
    path:``,
    component: SiteComponent,
    children: [
        { path: '', redirectTo:'landing-page', pathMatch:'full'},
        { path: 'landing-page', component: LandingPageComponent, pathMatch:'full'} 
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SiteRoutingModule { }