import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './container/admin/admin.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';

const routes: Routes = [
   {
    path:``,
    component: AdminComponent,
    children: [
        { path: '', redirectTo:'dashboard', pathMatch:'full'},
        { path: 'dashboard', component: DashboardComponent, pathMatch:'full'} 
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }